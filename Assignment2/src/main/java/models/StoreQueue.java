package models;
import java.util.LinkedList;
import java.util.Queue;

public class StoreQueue implements Runnable {

	int id;
	int availablePlaces;
	int totalProcessingTime = 0;
	int totalProcessingTimeOnInterval = 0;
	int remainingProcessingTime = 0;
	int totalWaitingTime = 0;
	int clientsServed = 0;
	String event = new String("");
	boolean open = true;
	Client actualClient;
	Queue<Client> myQueue;
	Thread t;

	public StoreQueue(int availablePlaces, int id) {
		this.id = id;
		this.availablePlaces = availablePlaces;
		this.myQueue = new LinkedList<Client>();
	}

	public void addClient(Client client) {
		this.myQueue.add(client);
		event = " <--- Clientul "+client.getID()+" s-a asezat la casa "+this.id;
		clientsServed++;
		totalWaitingTime += this.getRemainingProcessingTime();
	}

	public int getTotalProcessingTime() {
		return this.totalProcessingTime;
	}

	public int getPeopleOnQueueNumber() {
		return myQueue.size();
	}

	public int getAvailablePlaces() {
		return this.availablePlaces;
	}

	public Queue<Client> getClientsInQueue() {
		return this.myQueue;
	}

	public Client getClient(int id) {
		Client rez = null;

		for (Client c : myQueue) {
			if (c.getID() == id)
				return c;
		}

		return rez;
	}

	public int getAverageWaitingTime() {
		
		if(clientsServed != 0)
			return totalWaitingTime / clientsServed;
		else return -1;
	}

	public int getID() {
		return id;
	}
	
	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public boolean hasEmptyPlaces() {
		if (myQueue.size() <= availablePlaces)
			return true;
		else
			return false;
	}

	public int getRemainingProcessingTime() {
		remainingProcessingTime = 0;

		if (!myQueue.isEmpty())
			for (Client c : myQueue)
				remainingProcessingTime += c.getProcessingTime();

		return remainingProcessingTime;
	}

	public void run() {

		while (open == true) {
			if (myQueue.isEmpty()) {
				waitOneSecond();
			} else {
				while (myQueue.peek().getProcessingTime() != 0) {
					waitOneSecond();
					totalProcessingTime++;

					myQueue.peek().decrementProcessingTime();
				}

				event = " <--- Clientul "+myQueue.peek().getID()+" a parasit casa "+this.id;
				myQueue.remove();
				
			}
		}

	}

	public void start() {
		if (t == null) {
			t = new Thread(this, Integer.toString(this.id));
			t.start();
		}
	}

	public boolean isEmpty() {
		if (myQueue.isEmpty())
			return true;
		else
			return false;
	}

	public void closeQueue() {
		this.open = false;
	}

	private void waitOneSecond() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
