package models;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import views.Log;
import views.QueuesFrame;
import views.*;

public class Store implements Runnable{

	ArrayList<Client> toBeProcessed = new ArrayList<Client>();
	int currentTime = 0;
	int simulationTime = 0;
	Thread t;
	ArrayList<StoreQueue> queues = new ArrayList<StoreQueue>();
	int placesOnQueue = 4;
	QueuesFrame frame; 
	JFrame dialogBox = new JFrame();
	Log log = new Log();
	int peakHour = 0;
	int lastMaxWaitingTime = 0;
	int totalProcessingTimeOnInterval = 0;
	
	public Store(int simulationTime, int numberOfClients, int numberOfQueues, int placesOnQueue, int minArrivalTime, int maxArrivalTime, int maxProcessingTime)
	{
		this.simulationTime = simulationTime;
		this.placesOnQueue = placesOnQueue;
		Client c1 = new Client(minArrivalTime, maxArrivalTime, maxProcessingTime, numberOfClients+1);
		c1.arrivalTime = maxArrivalTime+1;
		
		for(int i=1; i<=numberOfClients; i++)
		{
			toBeProcessed.add(new Client(minArrivalTime, maxArrivalTime, maxProcessingTime, i));
		}
			toBeProcessed.add(c1);
		
		this.sortClients();
		
		this.initialiseStoreQueues(numberOfQueues);
		
		for(StoreQueue sq : queues)
			{
				sq.setEvent("");
				sq.start();
			}
		
		frame = new QueuesFrame(numberOfQueues);
		frame.setVisible(true);
		log.setVisible(true);

	}
	
	public void initialiseStoreQueues(int numberOfQueues)
	{
		for(int i=1; i<=numberOfQueues; i++)
			queues.add(new StoreQueue(placesOnQueue, i));
	}
	
	public boolean emptyQueueCheck()
	{		
		for(StoreQueue sq : queues)
		{
			if(sq.hasEmptyPlaces())
				return true;
		}
		
		return false;
	}
	
	public int getBestQueue()
	{
		int id = 0;
		int comparisonValue = 999;
		
		for(StoreQueue sq : queues)
		{
			if(sq.hasEmptyPlaces() && sq.getRemainingProcessingTime() < comparisonValue)
				{
					id = sq.getID();
					comparisonValue = sq.getRemainingProcessingTime();
				}
		}
		
		return id;
	}
	
	private void sortClients()
	{
		Collections.sort(toBeProcessed);
	}
	
	private void checkPeakHour()
	{
		for(StoreQueue sq : queues)
		{
			if(sq.getRemainingProcessingTime() >= lastMaxWaitingTime)
			{
				peakHour = currentTime;
				lastMaxWaitingTime = sq.getRemainingProcessingTime();
			}
		}
	}
	
	public int getPeakHour()
	{
		return this.peakHour;
	}
	
	public void updateLog()
	{
		for(StoreQueue sq : queues)
		{
			if(sq.getEvent()!="")
				{
					log.append("Moment timp: "+(currentTime-1)+sq.getEvent());
					sq.setEvent("");
				}
			else
				sq.setEvent("");
		}
	}
	
	public void run() 
	{
		int startingIndex = 0;
		
		while(currentTime < simulationTime  || startingIndex != toBeProcessed.size())
		{
			
			if(this.emptyQueueCheck() == true)
				
			{
				while(toBeProcessed.get(startingIndex).getArrivalTime()<=currentTime && this.emptyQueueCheck() == true)
				{
					queues.get(getBestQueue()-1).addClient(toBeProcessed.get(startingIndex));
					this.updateLog();
					if(startingIndex!=toBeProcessed.size()-1)	
						startingIndex++;
						else
							break;
				}
				
				checkPeakHour();
				waitOneSecond();
				currentTime++;
				
				if(startingIndex==toBeProcessed.size()-1)
					break;

			}				
			else
			{
				
				checkPeakHour();
				waitOneSecond();
				currentTime++;
			}
			this.updateLog();
			frame.setQueuesInfo(queues, currentTime);
			
			
		}
		
		while(true)
		{
			if(this.allQueuesDone())
			{
				for(StoreQueue sq : queues)
				{
					sq.closeQueue();
				}
				
				frame.printTimes(queues);
				JOptionPane.showMessageDialog(dialogBox, "Peak hour: "+this.getPeakHour());
				
				break;
			}
			else
			{
				
				checkPeakHour();
				waitOneSecond();
				currentTime++;
				this.updateLog();
				frame.setQueuesInfo(queues, currentTime);
				
			}
		}
	}
	
	public void Start()
	{
		if(t == null)
		{
			t = new Thread(this, "Store");
			t.start();
		}
	}

	private boolean allQueuesDone()
	{
		boolean result = true;
		
		for(StoreQueue sq : queues)
		{
			if(!sq.isEmpty())
				result = false;
		}
		
		return result;
	}
	
	private void waitOneSecond()
	{
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
