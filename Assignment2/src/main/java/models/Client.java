package models;

import java.util.Random;


public class Client implements Comparable<Client>{

	int processingTime = 0;
	int arrivalTime = 0;
	int id = 0;
	
	public Client(int minArrivalTime, int maxArrivalTime, int maxProcessingTime, int id)
	{
		Random rand = new Random();
		
		this.id = id;
		processingTime = rand.nextInt(maxProcessingTime) + 1;
		arrivalTime = rand.nextInt(maxArrivalTime) + minArrivalTime;
	}
	
	public int getID()
	{
		return this.id;
	}
	
	public int getArrivalTime()
	{
		return this.arrivalTime;
	}
	
	public int getProcessingTime()
	{
		return this.processingTime;
	}
	
	public void decrementProcessingTime()
	{
		if(processingTime>0)
		this.processingTime--;
	}

	public int compareTo(Client c) {
		
		if(this.getArrivalTime() > c.getArrivalTime())
			return 1;
		else if(this.getArrivalTime() < c.getArrivalTime())
			return -1;
		else return 0;
		
	}
}
