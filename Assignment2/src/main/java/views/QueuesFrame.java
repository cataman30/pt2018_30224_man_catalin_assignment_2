package views;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import models.Client;
import models.StoreQueue;

import java.awt.FlowLayout;
import java.awt.TextArea;

public class QueuesFrame extends JFrame {

	private JPanel contentPane;
	ArrayList<TextArea> areas = new ArrayList<TextArea>();
	Dimension textAreaDim = new Dimension(200,300);
	
	public QueuesFrame(int nr) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 708, 617);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		for(int i=0; i<nr; i++)
			areas.add(new TextArea());
		
		for(TextArea ta : areas)
		{
			ta.setPreferredSize(textAreaDim);
			contentPane.add(ta);
		}
	}
	
	public void setQueuesInfo(ArrayList<StoreQueue> queues, int currentTime)
	{
		for(int i=0; i<queues.size(); i++)
		{
			areas.get(i).setText("");
		}
		
		for(int j=0; j<queues.size(); j++)
		{
			areas.get(j).append("Coada "+(j+1)+"        Timp Curent: "+currentTime+"\nTimp ramas procesare: "+queues.get(j).getRemainingProcessingTime()+"\n----"
					+ "---------------------------------------\n");
			
			//for(int k=0; k<(queues.get(j).getClientsInQueue().size()); k++)
			{
				if(!queues.get(j).isEmpty())
				{
					for(Client c : queues.get(j).getClientsInQueue())
				areas.get(j).append("Clientul "+c.getID()+": "+c.getProcessingTime()+"\n");
					}
			}
			
		}
	}
	
	public void printTimes(ArrayList<StoreQueue> queues)
	{
		for(int i=0; i<queues.size(); i++)
		{
			if(queues.get(i).getAverageWaitingTime()>=0)
				areas.get(i).setText("Timp mediu asteptare: "+queues.get(i).getAverageWaitingTime()+"\n"+"Timp asteptare: "+queues.get(i).getTotalProcessingTime());
			else
				areas.get(i).setText("Casa nu a avut clienti");
		}
	}
}


