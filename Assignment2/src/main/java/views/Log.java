package views;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.TextArea;

public class Log extends JFrame {

	private JPanel contentPane;
	TextArea textArea = new TextArea();

	public Log() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 525, 581);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textArea.setBounds(10, 10, 487, 514);
		contentPane.add(textArea);
	}
	
	public void append(String event)
	{
		textArea.append(event+"\n");
	}
}
