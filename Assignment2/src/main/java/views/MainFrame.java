package views;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import models.Store;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField = new JTextField();;
	private JTextField textField_1 = new JTextField();;
	private JTextField textField_2 = new JTextField();;
	private JTextField textField_3 = new JTextField();;
	private JTextField textField_4 = new JTextField();;
	private JTextField textField_5 = new JTextField();;
	private JTextField textField_6 = new JTextField();;
	JLabel lblTimpSimulare = new JLabel("Timp Simulare:");
	JLabel lblNumarClienti = new JLabel("Numar Clienti:");
	JLabel lblNumarCozi = new JLabel("Numar Case:");
	JLabel lblLocuriInCoada = new JLabel("Locuri In Casa:");
	JLabel lblTimpMinimSosire = new JLabel("Timp Minim Sosire:");
	JLabel lblTimpMaximSosire = new JLabel("Timp Maxim Sosire:");
	JLabel lblTimpMaximProcesare = new JLabel("Timp Maxim Procesare:");
	JButton btnStartSimulare = new JButton("Start Simulare");
	
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 392, 374);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblTimpSimulare.setBounds(42, 37, 109, 16);
		contentPane.add(lblTimpSimulare);
		
		lblNumarClienti.setBounds(42, 66, 109, 16);
		contentPane.add(lblNumarClienti);
		
		lblNumarCozi.setBounds(42, 95, 109, 16);
		contentPane.add(lblNumarCozi);
		
		lblLocuriInCoada.setBounds(42, 124, 109, 16);
		contentPane.add(lblLocuriInCoada);
		
		lblTimpMinimSosire.setBounds(42, 153, 121, 16);
		contentPane.add(lblTimpMinimSosire);
		
		lblTimpMaximSosire.setBounds(42, 182, 121, 16);
		contentPane.add(lblTimpMaximSosire);
		
		lblTimpMaximProcesare.setBounds(42, 211, 136, 16);
		contentPane.add(lblTimpMaximProcesare);
		 
		textField.setBounds(208, 34, 116, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1.setColumns(10);
		textField_1.setBounds(208, 63, 116, 22);
		contentPane.add(textField_1);
		
		textField_2.setColumns(10);
		textField_2.setBounds(208, 92, 116, 22);
		contentPane.add(textField_2);
		
		textField_3.setColumns(10);
		textField_3.setBounds(208, 121, 116, 22);
		contentPane.add(textField_3);
		
		textField_4.setColumns(10);
		textField_4.setBounds(208, 150, 116, 22);
		contentPane.add(textField_4);
		
		textField_5.setColumns(10);
		textField_5.setBounds(208, 179, 116, 22);
		contentPane.add(textField_5);
		
		textField_6.setColumns(10);
		textField_6.setBounds(208, 208, 116, 22);
		contentPane.add(textField_6);
		btnStartSimulare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Store s1 = new Store(Integer.parseInt(textField.getText()), Integer.parseInt(textField_1.getText()), Integer.parseInt(textField_2.getText()),
						Integer.parseInt(textField_3.getText()), Integer.parseInt(textField_4.getText()), 
						Integer.parseInt(textField_5.getText()), Integer.parseInt(textField_6.getText()));
				
				s1.Start();
				
				
			}
		});
		
		btnStartSimulare.setBounds(42, 249, 282, 39);
		contentPane.add(btnStartSimulare);
	}
	
	
}
